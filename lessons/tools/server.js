import express from 'express';
import open from 'open';
import chalk from 'chalk';

/* eslint-disable no-console */
const port = 3010;
const app = express();

app.use(express.static('public'));

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log(chalk.green(`Starting lessons dev server on port http://localhost:${port}`))
    open(`http://localhost:${port}`);
  }
});

export const server = (args) => {
  console.log(args);
};

export default server;
