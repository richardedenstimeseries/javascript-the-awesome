import * as types from './actionTypes';
import gamePageApi from '../api/mock/gameApi';

export function loadGamePagesSuccess(gamePages) {
  return { type: types.LOAD_GAMEPAGE_SUCCESS, gamePages };
}

export function loadGamePages() {
  return function(dispatch) {
    return gamePageApi.getAllGamePages().then(gamePages => {
      dispatch(loadGamePagesSuccess(gamePages));
    }).catch(error => {
      throw(error);
    });
  };
}
