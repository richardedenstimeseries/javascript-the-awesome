import * as types from './actionTypes';
import level1Api from '../api/mock/level1Api';

export function loadLevel1Success(level1) {
  return { type: types.LOAD_LEVEL1_SUCCESS, level1 };
}

export function loadLevel1() {
  return function(dispatch) {
    return level1Api.getAllLevel1().then(level1 => {
      dispatch(loadLevel1Success(level1));
    }).catch(error => {
      throw(error);
    });
  };
}
