import React from 'react';
import TitleStory from './TitleStory';

class HomePage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      logo: '/img/nintendo.jpg'
    };

  }


  render() {
    return (
       <div className="homepage">
        <div className="nintendo-logo"><img src={this.state.logo} /></div>
       </div>
    );
  }
}

export default HomePage;
