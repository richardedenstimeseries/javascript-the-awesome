import React from 'react';
import TitleStory from './TitleStory';

class TitlePage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      video: '/videos/title.mp4'
    };

    this.videoEnded = this.videoEnded.bind(this);
  }

  videoEnded() {
    window.location.href= "/intro";
  }

  render() {
    return (
       <div className="intro">
        <TitleStory video={this.state.video} videoEnded={this.videoEnded} />
       </div>
    );
  }
}

export default TitlePage;
