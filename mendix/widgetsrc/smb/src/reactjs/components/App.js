import React from 'react';
import PropTypes from 'prop-types';
import Header from './common/Header';
import KeyboardControl from "./common/KeyboardControl";

// Set the super mario bros window core.
window.smb = {
  core: {
    keyboard: {
      leftarrow: false,
      rightarrow: false,
      uparrow: false,
      downarrow: false
    },
    hero: {
      bottom: ((85 * window.innerHeight) / 781),
      left: 0,
      running: 0,
      directionh: ''
    }
  }
};

class App extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <KeyboardControl props={this.props} />
        <Header />
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array,PropTypes.object])
};

export default App;
