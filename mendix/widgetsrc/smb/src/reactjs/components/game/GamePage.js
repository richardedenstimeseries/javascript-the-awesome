import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/gamePage';

// Engine
import Physics from './engine/Physics';
import Statistics from './engine/Statistics';
import GameLoop from './engine/GameLoop';
import KeyboardPressed from './engine/KeyboardPressed';

// Hero's
import Mario from './heros/Mario';

// Levels
import Backgrounds1 from './backgrounds/background1';

// Levels
import Level1 from './levels/Level1';

class GamePage extends React.Component {
  constructor(props, context) {
   super(props, context);
 }

  render() {
    return (
       <div className="game-fullscreen">
         <GameLoop>
          <Backgrounds1 />
          <Level1 />
          <Physics>
            <Mario />
          </Physics>
          <Statistics />
          <KeyboardPressed />
         </GameLoop>
       </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    state: state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GamePage);
