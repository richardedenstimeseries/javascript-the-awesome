import React from 'react';

class Background1 extends React.Component {
  constructor(props, context) {
   super(props, context);
   this.imgRef = React.createRef();
   this.state = {
     img: ''
   };
 }

 componentDidMount() {
  let backgroundImage = new Image();
  let self = this;
  backgroundImage.onload = function() {
    self.setState({img: this.src});
  };
  backgroundImage.src = '/levels/level1.jpg';
 }

  render() {
    return (
       <div className="game-background-level1">
        <img src={this.state.img} height="100%" />
       </div>
    );
  }
}

export default Background1;
