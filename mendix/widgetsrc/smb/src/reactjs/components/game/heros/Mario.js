import React, {PropTypes} from 'react';
import {Link} from 'react-router-dom';

const Mario = () => {

  let positionStyle = {
    bottom: window.smb.core.hero.bottom + 'px',
    left: window.smb.core.hero.left + 'px'
  };

  if (window.smb.core.hero.running !== 0) {
    window.smb.core.hero.running = window.smb.core.hero.running - 0.05;
    if (window.smb.core.hero.directionh == 'left') {
      window.smb.core.hero.left -= window.smb.core.hero.running;
      positionStyle.left = window.smb.core.hero.left + 'px';
    }
    if (window.smb.core.hero.directionh == 'right') {
      window.smb.core.hero.left += window.smb.core.hero.running;
      positionStyle.left = window.smb.core.hero.left + 'px';
    }
  }

  if (window.smb.core.keyboard.leftarrow) {
    window.smb.core.hero.direction = 'left';
    window.smb.core.hero.running += 0.2;
    window.smb.core.hero.running = window.smb.core.hero.running - 0.05;
    window.smb.core.hero.left -= window.smb.core.hero.running;
    positionStyle.left = window.smb.core.hero.left + 'px';
  }
  if (window.smb.core.keyboard.rightarrow) {
    window.smb.core.hero.direction = 'right';
    window.smb.core.hero.running += 0.2;
    window.smb.core.hero.running = window.smb.core.hero.running - 0.05;
    window.smb.core.hero.left += window.smb.core.hero.running;
    positionStyle.left = window.smb.core.hero.left + 'px';
  }
  if (window.smb.core.keyboard.uparrow) {
    window.smb.core.hero.bottom += 1;
    positionStyle.bottom = window.smb.core.hero.bottom + 'px';
  }

  return (
      <div className="game-mario mario" style={positionStyle}></div>
  );
};

export default Mario;
