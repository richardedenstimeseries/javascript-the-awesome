import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../../actions/level1Actions';

class Level1 extends React.Component {
  constructor(props, context) {
   super(props, context);
   this.canvasRef = React.createRef();

   this.state = {
     heroX: 2,
     heroY: 19,
     currentColor: {
       backgroundColor: '#fff'
     },
     c: [0,0,0]
   };
  }

  componentDidUpdate(prevProps, prevState) {
    let canvas = this.canvasRef.current;
    let ctx = canvas.getContext('2d');
    let img = new Image;
    let self = this;
    img.onload = function() {
        ctx.drawImage(img, 0, 0, 400, 40);
    };
    img.src = '/levels/level1.png';
  }

  render() {
    return (
       <div className="game-level-view">
         <canvas ref={this.canvasRef} width="800" height="100" />
         <div style={this.state.currentColor}></div>
         <label>R: {this.state.c[0]} - G: {this.state.c[1]} - B: {this.state.c[2]}</label>
       </div>
    );
  }
}

Level1.propTypes = {
  level1: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    state: state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Level1);
