import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class Statistics extends React.Component {
  constructor(props, context) {
   super(props, context);
 }

  render() {
    return (
       <div className="game-statistics">
          <div className="header">Super Mario Bros - Reactified.</div><br/>
          <strong>Global ticker?</strong><br/>
          {this.props.globalTicker}
          <br/><br/>
          <strong>How many 100 milisecond on current screen?</strong><br/>
          {this.props.ticker}
          <br/><br/>
          <strong>How much has been scrolled?</strong><br/>
          {this.props.widthscreen} - {this.props.widthscrolled}
      </div>
    );
  }
}

Statistics.propTypes = {
  globalTicker: PropTypes.number,
  ticker: PropTypes.number,
  widthscreen: PropTypes.number,
  widthscrolled: PropTypes.number
};

export default Statistics;
