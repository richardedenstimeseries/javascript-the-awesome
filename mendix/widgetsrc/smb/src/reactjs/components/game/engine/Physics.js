import React from 'react';
import PropTypes from 'prop-types';

class Physics extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
        positionX: 100.0,
        positionY: 175.0,
        velocityX: 4.0,
        velocityY: 0.0,
        gravity: 0.5,
        onGround: false
    };
  }

  startJump()
  {
      if(this.state.onGround)
      {
          this.setState(
            Object.assign({},
              this.state,
              {
                positionY: 175.0,
                velocityY: -12.0,
                onGround: false
              }
            )
          );
      }
  }

  endJump()
  {
        if(this.state.velocityY < -6.0) {
          this.setState(
              Object.assign({},
                this.state,
                { velocityY: -6.0 }
              )
          );
        }
  }


  render() {

    // Determain velocityX.
    if(this.state.positionX < 10 || this.state.positionX > 190) {
        let velocityXVal = this.state.velocityX;
        this.setState(
            Object.assign({},
              this.state,
              {
                velocityX: velocityXVal *= -1
              }
            )
        );
    }

    // Determain on the ground
    if(this.state.positionY > 175.0)
    {
        this.setState(
          Object.assign({},
            this.state,
            {
              positionY: 175.0,
              velocityY: 0.0,
              onGround: true
            }
          )
        );
    }

    return (
       <div>
        {
           React.Children.map(this.props.children,
            child => React.cloneElement(child,
              {
                positionX: this.state.positionX,
                positionY: this.state.positionY,
                velocityX: this.state.velocityX,
                velocityY: this.state.velocityY,
                gravity: this.state.gravity,
                onGround: this.state.onGround
              }
            )
          )
        }
        <div className="game-physics">
            <div className="header">Physics.</div><br/>
            <strong>X and Y</strong><br/>
            {this.state.positionX} - {this.state.positionY}
            <br/><br/>
            <strong>Velocity X and Y</strong><br/>
            {this.state.velocityY} - {this.state.velocityY}
            <br/><br/>
            <strong>Gavity / On the ground?</strong><br/>
            {this.state.gravity} - {this.state.onGround}
        </div>
       </div>
    );
  }
}

Physics.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array,PropTypes.object]),
  positionX: PropTypes.number,
  positionY: PropTypes.number,
  velocityX: PropTypes.number,
  velocityY: PropTypes.number,
  gravity: PropTypes.number,
  onGround: PropTypes.bool
};

export default Physics;
