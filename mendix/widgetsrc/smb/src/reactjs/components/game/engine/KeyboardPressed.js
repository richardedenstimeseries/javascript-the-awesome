import React from 'react';

let tick = 0;

const KeyboardPressed = () => {

  let leftarrow = '';
  let rightarrow = '';
  let uparrow = '';
  let downarrow = '';
  if (window.smb.core.keyboard.leftarrow) {
    leftarrow = 'Left arrow';
  }
  if (window.smb.core.keyboard.rightarrow) {
    rightarrow = 'Right arrow';
  }
  if (window.smb.core.keyboard.uparrow) {
    uparrow = 'Up arrow';
  }
  if (window.smb.core.keyboard.downarrow) {
    downarrow = 'Down arrow';
  }

  tick += 1;

  return (
      <div className="game-keyboard-pressed">
        Tick:: {tick} <br/>
        Keyboard: <br/>
        {leftarrow} <br/>
        {rightarrow} <br/>
        {uparrow} <br/>
        {downarrow} <br/>
      </div>
  );
};

export default KeyboardPressed;

