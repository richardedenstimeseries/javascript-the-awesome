import React from 'react';
import PropTypes from 'prop-types';

class GameLoop extends React.Component {

  constructor(props, context) {

    super(props, context);

    this.state = {
        levelscroll: 0,
        ticker: 0,
        globalTicker: 0,
        internalSeconds: 0,
        settings: {
          amount: 0,
          amountAddedMax: window.innerWidth,
          amountAdded: 0,
          ticker: 0,
          globalTicker: 0,
          startNextPart: true
        }
    };

  }

  tick(e) {
    // A global ticker, how many ticks have been done.
    let settings = Object.assign({}, this.state.settings);
    settings.globalTicker = settings.globalTicker + 1;

    // We need to check if the screen has moved a complete width of its screen.
    if (settings.startNextPart === true) {
        if (settings.amountAdded === settings.amountAddedMax) {
          settings.amountAdded = 0;
          settings.startNextPart = false;
          settings.ticker = 0; // 15 seconds done.
        } else {
          settings.amountAdded = settings.amountAdded + 1;
          settings.amount = settings.amount + 1;
        }
    } else {
        // If the parameters do not match yet we need to see how long someone is on the current screen.
        settings.startNextPart = false;
        // If this is more then 15 seconds, we move the screen gradually to the left.
        if (settings.ticker === 150) {
          settings.amountAdded = 0;
          settings.startNextPart = true;
          settings.ticker = 0; // 15 seconds done.
        }
        settings.ticker = settings.ticker + 1;
    }
    this.setState( {
        levelscroll: settings.amount,
        ticker: settings.ticker,
        amountAddedMax: settings.amountAddedMax,
        amountAdded: settings.amountAdded,
        globalTicker: settings.globalTicker,
        settings: settings
    });
}

  render() {

    // Change the state with a setTimeout
    setTimeout(this.tick.bind(this), 50);

    return (
       <div>
        {
           React.Children.map(this.props.children,
            child => React.cloneElement(child,
              {
                levelscroll: this.state.levelscroll,
                ticker: this.state.ticker,
                widthscreen: this.state.amountAddedMax,
                widthscrolled: this.state.amountAdded,
                globalTicker: this.state.globalTicker
              }
            )
          )
        }
       </div>
    );
  }
}

GameLoop.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array,PropTypes.object])
};

export default GameLoop;
