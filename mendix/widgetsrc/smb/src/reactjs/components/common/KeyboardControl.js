

import React from 'react';

const KeyboardControl = ({props}) => {

  let keyControl = ((e) => {
    // Multiple key
    e = e || event; // to deal with IE
    if (e.keyCode == 37) {
      window.smb.core.keyboard.leftarrow = e.type == 'keydown';
    }
    if (e.keyCode == 39) {
      window.smb.core.keyboard.rightarrow = e.type == 'keydown';
    }
    if (e.keyCode == 38) {
      window.smb.core.keyboard.uparrow = e.type == 'keydown';
    }
    if (e.keyCode == 40) {
      window.smb.core.keyboard.downarrow = e.type == 'keydown';
    }
    let pathname = window.location.pathname;
    // Go to new page.
    if (e.type == 'keydown') {
      if (e.keyCode==13 || e.keyCode==50) {
        switch(pathname) {
            case "/":
              window.location.href = '/title';
              break;
            case "/title":
              window.location.href = '/intro';
              break;
            case "/intro":
              window.location.href = '/game';
              break;
            case "/game":
              window.location.href = '/';
              break;
          default:
            break;
        }
      }
    }
  }).bind(this);

  document.addEventListener("keydown", keyControl, false);
  document.addEventListener("keyup", keyControl, false);

  return (
    <div className="keyboard">
    </div>
  );

};

export default KeyboardControl;
