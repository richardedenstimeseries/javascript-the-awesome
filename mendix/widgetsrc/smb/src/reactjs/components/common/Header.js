import React from 'react';
import {NavLink} from 'react-router-dom';

const Header = (props) => {
  return (
    <nav>
      <NavLink to="/" activeClassName="active">Home</NavLink>
      {" | "}
      <NavLink to="/intro" activeClassName="active">Intro</NavLink>
      {" | "}
      <NavLink to="/game" activeClassName="active">Game</NavLink>
      {" | "}
      <NavLink to="/courses" activeClassName="active">Courses</NavLink>
      {" | "}
      <NavLink to="/about" activeClassName="active">About</NavLink>
    </nav>
  );
};

export default Header;
