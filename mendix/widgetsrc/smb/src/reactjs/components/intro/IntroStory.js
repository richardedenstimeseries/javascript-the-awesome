import React from 'react';
import PropTypes from 'prop-types';

const IntroStory = ({video, videoEnded}) => {
  return (
    <video className="fullscreenvideo" width="100%" height="100%" playsInline="true" autoPlay="true" onEnded={videoEnded}>
      <source src={video} type="video/mp4" />
    </video>
  );
};

IntroStory.propTypes = {
  video: PropTypes.string.isRequired,
  videoEnded: PropTypes.func.isRequired
};

export default IntroStory;

