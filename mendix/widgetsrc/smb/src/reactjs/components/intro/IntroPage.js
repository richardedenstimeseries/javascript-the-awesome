import React from 'react';
import IntroStory from './IntroStory';

class IntroPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      video: '/videos/intro.mp4'
    };

    this.videoEnded = this.videoEnded.bind(this);
  }

  videoEnded() {
    window.location.href= "/game";
  }

  render() {
    return (
       <div className="intro">
        <IntroStory video={this.state.video} videoEnded={this.videoEnded} />
       </div>
    );
  }
}

export default IntroPage;
