import {combineReducers} from 'redux';
import context from './contextReducer';
import courses from './courseReducer';
import authors from './authorReducer';
import level1 from './level1Reducer';

const rootReducer = combineReducers({
  context,
  courses,
  authors,
  level1
});

export default rootReducer;
