import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function level1Reducer(state = initialState.level1, action) {
  switch(action.type) {
    case types.LOAD_LEVEL1_SUCCESS:
      return action.level1;

    default:
      return state;
  }
}
