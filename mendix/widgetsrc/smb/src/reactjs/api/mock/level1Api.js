import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const level1 = [
  {
    id: "react-flux-building-applications",
    title: "Building Applications in React and Flux",
    watchHref: "http://www.pluralsight.com/level1/react-flux-building-applications",
    authorId: "cory-house",
    length: "5:08",
    category: "JavaScript"
  }
];

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (level1) => {
  return replaceAll(level1.title, ' ', '-');
};

class Level1Api {
  static getAllLevel1() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], level1));
      }, delay);
    });
  }
}

export default Level1Api;
