/*eslint-disable import/default */
import '@babel/polyfill';
import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Actions
import {loadContext} from './actions/contextAction';
import {loadCourses} from './actions/courseActions';
import {loadAuthors} from './actions/authorActions';
import {loadLevel1} from './actions/level1Actions';

// Components
import App from './components/App';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import CoursesPage from './components/course/CoursesPage';
import ManageCoursePage from './components/course/ManageCoursePage';
import GamePage from './components/game/GamePage';
import IntroPage from './components/intro/IntroPage';
import TitlePage from './components/title/TitlePage';

// CSS and SCSS
import './styles/sass/styles.scss';

const store = configureStore();
store.dispatch(loadContext());
store.dispatch(loadCourses());
store.dispatch(loadAuthors());
store.dispatch(loadLevel1());

render(
  <Provider store={store}>
    <Router>
      <App>
        <Route path="/" component={HomePage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/course" component={ManageCoursePage} />
        <Route path="/course/:id" component={ManageCoursePage} />
        <Route path="/courses" component={CoursesPage} />
        <Route path="/intro" component={IntroPage} />
        <Route path="/title" component={TitlePage} />
        <Route path="/game" component={GamePage} />
      </App>
    </Router>
  </Provider>,
  document.getElementById('root')
);
